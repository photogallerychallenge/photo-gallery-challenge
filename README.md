## Photo gallery challenge

You're training for a Back-end developer position at Netcracker,
and for this challenge you'll have 10-20 hours per week, during a 2 month period, 
to develop a project from scratch working in a 3 person team.

**We'll be looking to assess the following skills:**

- Comprehension and Problem-solving.
- Comprehensible code writing and organization.
- Knowledge in Java programming.
- Use of Java Runtime and its Libraries.
- Intelligible and useful documentation writing, like READMEs 
	and useful code comments.
- Good Development Practices like automated tests.
- Comprehensible English reading and writing.
- You can submit answers to any Software Distributed Management System 
	(e.g. GitHub) repository, or by email if you require privacy or confidentiality.
	We'd like all documentation to be written in English, 
	but if you're uncomfortable with it, it's okay to be in Portuguese.

---

**In this project you'll be able to learn or reinforce knowledge of:**

- Creating a project from scratch.
- Organizing and splitting requirements in a work team.
- Use of Git repository.
- Build Java microservices using Spring.
- Dependency management using Maven.
- Create REST APIs.
- Exceptions and error handling.
- Use of google Auth and a 3rd party APIs, Google's 
	[Library APIs](https://developers.google.com/photos/library/guides/overview).
- Create and manage I/O transactions in a database.
- Create unit test.

---

## Requirements:

Travelers Group is a worldwide organization where professionals and non-professionals
photographers and artists (all of whom are called publishers) share their work to the internet,
to a website. For each photo and album page, there will be ads of Travelers Group partners being displayed.
Those ads are monetized based on the number of visualizations,
and a percentage of this revenue will be shared with publishers proportional to the visualization of their materials.

**Travelers Group wants an application that allows:**

1. Publishers to upload pictures in different shared albums,
	so that Travelers Group can share the albums in the website.
2. External Users to view content from all the albums shared by publishers,
	so that Travelers Group is able to count the toal visualizations for each album.
3. Travelers Group wants to publish weekly collages of 4 pictures, getting the most 
	popular picture from user's 4 most popular albums,
	so that publishers can be aware of trends among users.
4. Advertisers need a report about most popular photos and albums, in order to be aware
	and adjust their advertisement strategy accordingly.
5. Advertisers wants a report of all their expenses for each album visualization.
6. Travelers Groups wants a report of all incomes and outcomes
	with advertisers and publishers.
	- Aggregated total.
	- Per category.
	- Income per Advertisers.
	- Expenses per Publishers.
7. Travelers Groups wants a report of all visualizations so that the company knows 
	the activity of the users in the site.
8. Users should be able to search albums and photos per categories.
	Which implies that publishers need to add hashtags to each album and photo.
9. Travelers Groups wants a report of the site most popular categories.

---

## Reference Guide:

For additional reference and information about the technologies that'll be used throughout
this challenge refer to **[Reference Guide](https://drive.google.com/open?id=1cyItk7Xyspasf2de_n2Wn-WWXSQg6bGz)** 
that has a list of useful resources and links that cover the basics, all the way up 
to more detailed concepts about technologies.


---

## Desafio Galeria de Fotos

Você está aplicando para um programa de treinamento para vagas de desenvolvedor
backend jr. na Netcracker. Neste desafio será necessário completar cerca de 
10-20 horas por semana durante 2 meses atuando em times de 3 pessoas.

**Procuraremos avaliar as seguintes habilidades:**

- Capacidade de resolução de problemas.
- Legibilidade do código e organização.
- Conhecimentos na linguagem Java.
- Utilização do Runtime do Java e de suas Bibliotecas.
- Documentação do código (como escrita de READMEs, comentários nos códigos, 
	detalhes do commit).
- Boas práticas de desenvolvimento, como testes automatizados.
- Boa capacidade de leitura e escrita na língua inglesa.
- Você pode submeter as respostas em qualquer sistema distribuído de gerenciamento 
	de software num repositório (por exemplo o GitHub), ou por email se necessitar de 
	confidencialidade e privacidade. Gostaríamos que toda documentação seja feita em inglês 
	(fique à vontade para utilizar alguma ferramenta de tradução, como o Google 
	Tradutor caso precise), mas senão se sentir confortável poderá documentar em 
	Português.

---

**Durante este projeto você terá a oportunidade de desenvolver as seguintes
competências:**

- Desenvolver um projeto do zero.
- Trabalho em equipe.
- Utilização do Git para criação e gerenciamento de software.
- Construir microserviços utilizando o Spring.
- Controle de dependências utilizando o Maven.
- Criação de APIs RESTful.
- Gerenciamento de Exceções e erros.
- Utilização do serviço de autenticação do Google e APIs de terceiros,
	Google's [Library APIs](https://developers.google.com/photos/library/guides/overview).
- Criar e gerenciar operações num banco de dados.
- Criação de testes unitários.

---

## Requisitos:

A Travelers Group é uma agência internacional onde fotógrafos profissionais e amadores 
e artistas (todos apelidados de publishers) publicam os seus trabalhos. Para cada 
página dos albuns e das fotos, propagandas são exibidas por patrocinadores, e a receita das propagandas 
a com base nas propagandas é baseada no número de vizualuzações. Um percentual desta receita recebida pela empresa
é distribuída para os publicadores de forma proporcional as visualizações de suas publicações.

**Travelers Group quer uma publicação que permita:**

1. Um Publisher fazer uploads em diferentes albuns, para que a Travelers Group 
	possa compartilhar o album no site.
2. Um usuário externo visualizar o conteúdo de todos os albuns compartilhados por 
	Publishers, para que a Travelers Group possa contabilizar a quantidade de 
	visualizações para cada album.
3. A Travelers Group gostaria de publicar semanalmente uma coleção de 4 fotos 
	mais visualizadas dos 4 albuns mais populares para cada usuário para que os 
	Publishers saibam quais conteúdos os usuários mais gostariam de ver.
4. Os Patrocinadores precisam de um relatório de ranking das fotos e albuns mais 
	populares entre usuário, para adaptarem sua estratégia de acordo.
5. Os Pratrocinadores gostariam de ter um relatório dos gastos para cada visualização
	de album.
6. A Travelers Groups gostaria de um relatório de balanço, com gastos e receitas
	envolvendo patrocinadores e publishers.
	- Um agregado total
	- Por categoria
	- Receita por patrocinador
	- Gastos por publisher
7. A empresa também gostaria de um relatório das visualizações,
	para que ela saiba o histórico de atividades dos usuários no site.
8. Como usuário, gostaria de buscar albuns por categoria, o que implica que os publishers
	precisam adicionar hashtags (categorizar) cada album e foto postados.
9. A Travelers groups gostaria de um relatório das categorias mais populares.

---

## Guia de Refência:

Para mais informações e referências sobre as tecnologias utilizadas, desde conceitos básicos
até tópicos mais avançados, por favor consultar o **[Guia de referência](https://drive.google.com/open?id=1cyItk7Xyspasf2de_n2Wn-WWXSQg6bGz)**.
Lá você também encontrará alguns recursos adicionais que poderão ajudá-los durante o projeto.